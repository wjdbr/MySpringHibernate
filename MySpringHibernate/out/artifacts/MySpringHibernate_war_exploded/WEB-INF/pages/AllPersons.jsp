<%--
  Created by IntelliJ IDEA.
  User: wjdbr
  Date: 15/7/9
  Time: 下午7:53
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
    <h1>一共有${allpersons.size()}个人</h1>
    <c:if test="${!empty allpersons}">
        <table class="tg">
            <tr>
                <th width="80">Person ID</th>
                <th width="120">Person Name</th>
                <th width="120">Person Country</th>
            </tr>
            <c:forEach items="${allpersons}" var="person">
                <tr>
                    <td>${person.id}</td>
                    <td>${person.name}</td>
                    <td>${person.country}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <form action="/addPerson">
        <input type="submit" value="Add"/>
    </form>
</body>
</html>
