package service;

import dao.PersonDao;
import dao.PersonDaoImpl;
import model.PersonEntity;

import java.util.List;

/**
 * Created by wjdbr on 15/7/9.
 */
public class PersonServiceImpl implements PersonService {
    private PersonDao personDao;

    public PersonServiceImpl() {
        this.personDao = new PersonDaoImpl();
    }

    @Override
    public List<PersonEntity> listPersons() {
        return this.personDao.listPersons();
    }

    @Override
    public void addPerson(PersonEntity p) {
        this.personDao.addPerson(p);
    }
}
