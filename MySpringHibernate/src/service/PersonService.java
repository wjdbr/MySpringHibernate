package service;

import model.PersonEntity;

import java.util.List;

/**
 * Created by wjdbr on 15/7/9.
 */
public interface PersonService {
    public List<PersonEntity> listPersons();
    public void addPerson(PersonEntity p);
}
