package global;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.List;

/**
 * Created by wjdbr on 15/7/10.
 */
public class HibernateHelper {

    private static final SessionFactory ourSessionFactory;
    private static final ServiceRegistry serviceRegistry;
    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            ourSessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static List getResultList(String sql,Class T) {
        final Session session = getSession();
        List rlist = null;
        try {
            final Query query = session.createSQLQuery(sql).addEntity(T);
            rlist = query.list();
        } finally {
            session.close();
        }
        return rlist;
    }

    public static boolean insert(Object entity) {
        try {
            final Session session = getSession();
            Transaction t = session.beginTransaction();
            session.save(entity);
            t.commit();
            session.close();
            return true;
        } catch (HibernateException e){
            e.printStackTrace();
            return false;
        }
    }

    public static boolean update(Object entity) {
        try {
            final Session session = getSession();
            Transaction t = session.beginTransaction();
            session.update(entity);
            t.commit();
            session.close();
            return true;
        } catch (HibernateException e){
            e.printStackTrace();
            return false;
        }
    }

    public static boolean delete(Object entity) {
        try {
            final Session session = getSession();
            Transaction t = session.beginTransaction();
            session.delete(entity);
            t.commit();
            session.close();
            return true;
        } catch (HibernateException e){
            e.printStackTrace();
            return false;
        }
    }
}
