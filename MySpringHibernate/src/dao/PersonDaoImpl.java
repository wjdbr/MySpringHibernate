package dao;

import global.HibernateHelper;
import model.PersonEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.List;

/**
 * Created by wjdbr on 15/7/9.
 */
public class PersonDaoImpl implements PersonDao {
    private static final SessionFactory ourSessionFactory;
    private static final ServiceRegistry serviceRegistry;
    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            ourSessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

//    @Override
//    public List<PersonEntity> listPersons() {
//        final Session session = getSession();
//        List<PersonEntity> listP = new ArrayList<>();
//        try {
//            final Query query = session.createQuery("from " + "model.PersonEntity");
//            for (Object o : query.list()) {
//                PersonEntity pe = (PersonEntity)o;
//                listP.add(pe);
//                System.out.println("  " + pe.getId() + " " + pe.getName() + " " + pe.getCountry());
//            }
//        } finally {
//            session.close();
//        }
//        return listP;
//    }
    @Override
    public List<PersonEntity> listPersons() {
        String sql = "select id,name,country from person";
        List<PersonEntity> persons = HibernateHelper.getResultList(sql,PersonEntity.class);
        return persons;
    }

    @Override
    public void addPerson(PersonEntity p) {
//        String sql = String.format("insert into person(id,name,country) values(null,%s,%s)",p.getName(),p.getCountry());
//        Session s = HibernateHelper.getSession();
        HibernateHelper.insert(p);
    }
}
