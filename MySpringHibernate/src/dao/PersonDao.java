package dao;

import model.PersonEntity;

import java.util.List;

/**
 * Created by wjdbr on 15/7/9.
 */
public interface PersonDao {
    List<PersonEntity> listPersons();
    void addPerson(PersonEntity p);
}
