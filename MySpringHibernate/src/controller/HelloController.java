package controller;

import model.PersonEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import service.PersonService;
import service.PersonServiceImpl;

import java.util.List;

/**
 * Created by wjdbr on 15/7/9.
 */

@Controller
public class HelloController {
    private PersonService personService;

    public HelloController() {
        this.personService = new PersonServiceImpl();
    }

    @RequestMapping(value="/")
    public String firstPage(Model model) {
        List<PersonEntity> allPersons = personService.listPersons();
        model.addAttribute("allpersons",allPersons);
        return "AllPersons";
    }

    @RequestMapping(value="/persons")
    public String listPersons(Model model) {
        List<PersonEntity> allPersons = personService.listPersons();
        model.addAttribute("allpersons",allPersons);
        return "AllPersons";
    }

    @RequestMapping(value = "/addPerson")
    public String addPerson() {
        return "AddPerson";
    }

    @RequestMapping(value = "/addPersonOpt")
    public String addPerson(@RequestParam("name") String name,@RequestParam("country") String country) {
        PersonEntity pe = new PersonEntity(name,country);
        personService.addPerson(pe);
        return "redirect:/persons";
    }

}
